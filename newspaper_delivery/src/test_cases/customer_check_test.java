package test_cases;

import customer.customer_check;
import junit.framework.TestCase;

public class customer_check_test extends TestCase {
	
	public void testCorrect001()
    {
        
        assertEquals(true,customer_check.isNameCorrect("li"));
        assertEquals(true,customer_check.isContactCorrect("9450418"));
        assertEquals(true, customer_check.isAddressCorrect("abcdefghij"));
    }
    public void testCorrect002()
    {
        
        assertEquals(true,customer_check.isNameCorrect("leeeeeeeeeeeeeeeeeeeeeeeeeeeee"));
        assertEquals(true,customer_check.isContactCorrect("9450418203"));
        assertEquals(true, customer_check.isAddressCorrect("chotachowkshahjahanpuruttarp"));

    }
    public void testCorrect003()
    {
       
        assertEquals(false,customer_check.isNameCorrect("l"));
        assertEquals(false,customer_check.isContactCorrect("945041"));
        assertEquals(false, customer_check.isAddressCorrect("abcdefghi"));

    }
    public void testCorrect004()
    {
        assertEquals(false,customer_check.isNameCorrect("abcdefghijklmnopqrstuvwxyzasdfg"));
        assertEquals(false,customer_check.isContactCorrect("94504182031"));
        assertEquals(false, customer_check.isAddressCorrect("abcdefghijklmnopqrstuvwxyzasdfgabcdefghijklmnopqrstu"));

    }


}
