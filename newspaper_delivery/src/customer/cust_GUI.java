package customer;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import connection.db_connection;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JScrollPane;

public class cust_GUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField_ID;
	private JTextField textField_NAME;
	private JTextField textField_CONTACT;
	private JTable table;

	/**
	 * Launch the application.
	 */
	Cust_Impl obj = new Cust_Impl();
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					cust_GUI frame = new cust_GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	Connection conn = null;
	/**
	 * Create the frame.
	 */
	public cust_GUI() {
		conn = db_connection.getConn();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 946, 620);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCustomer = new JLabel("  CUSTOMER");
		lblCustomer.setBounds(440, 34, 94, 31);
		contentPane.add(lblCustomer);
		
		JLabel ID = new JLabel("ID");
		ID.setBounds(35, 125, 45, 13);
		contentPane.add(ID);
		
		JLabel NAME = new JLabel("NAME");
		NAME.setBounds(35, 168, 45, 13);
		contentPane.add(NAME);
		
		JLabel CONTACT_NO = new JLabel("CONTACT NO");
		CONTACT_NO.setBounds(35, 207, 79, 13);
		contentPane.add(CONTACT_NO);
		
		JLabel DeliveryArea = new JLabel("DELIVERY AREA");
		DeliveryArea.setBounds(35, 242, 79, 13);
		contentPane.add(DeliveryArea);
		
		JLabel Address = new JLabel("ADDRESS");
		Address.setBounds(35, 286, 45, 13);
		contentPane.add(Address);
		
		textField_ID = new JTextField();
		textField_ID.setBounds(130, 122, 96, 19);
		contentPane.add(textField_ID);
		textField_ID.setColumns(10);
		
		textField_NAME = new JTextField();
		textField_NAME.setColumns(10);
		textField_NAME.setBounds(130, 165, 96, 19);
		contentPane.add(textField_NAME);
		
		textField_CONTACT = new JTextField();
		textField_CONTACT.setColumns(10);
		textField_CONTACT.setBounds(130, 204, 96, 19);
		contentPane.add(textField_CONTACT);
		
		JComboBox comboBox_DELIVERY_AREA = new JComboBox();
		comboBox_DELIVERY_AREA.setMaximumRowCount(4);
		comboBox_DELIVERY_AREA.setModel(new DefaultComboBoxModel(new String[] {"monksland","golden island","AIT","garry castle"}));
		comboBox_DELIVERY_AREA.setSelectedIndex(1);;
		comboBox_DELIVERY_AREA.setBounds(130, 238, 96, 21);
		contentPane.add(comboBox_DELIVERY_AREA);
		
		JTextArea textArea_ADDRESS = new JTextArea();
		textArea_ADDRESS.setBounds(130, 280, 96, 22);
		contentPane.add(textArea_ADDRESS);
		
		JButton btnNewButton = new JButton("ADD");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = textField_NAME.getText();
				String contactno = textField_CONTACT.getText();
				String deliveryArea = (String)comboBox_DELIVERY_AREA.getSelectedItem().toString();
				String address = textArea_ADDRESS.getText();
				obj.add_cust(name, contactno, deliveryArea, address);
				try {
					ResultSet resultdata = obj.view();
				
			} catch (Exception e1) {
				e1.printStackTrace();
			}
				
			}
		});
		btnNewButton.setBounds(78, 460, 85, 21);
		contentPane.add(btnNewButton);
		
		JButton btnUpdate = new JButton("UPDATE");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int cid = Integer.parseInt(textField_ID.getText());
				String name = textField_NAME.getText();
				String contactno = textField_CONTACT.getText();
				String deliveryArea = (String)comboBox_DELIVERY_AREA.getSelectedItem().toString();
				String address = textArea_ADDRESS.getText();
				obj.update(cid ,name, contactno, deliveryArea, address);
				try {
					ResultSet resultdata = obj.view();
				
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			}
		});
		btnUpdate.setBounds(304, 460, 85, 21);
		contentPane.add(btnUpdate);
		
		JButton btnView = new JButton("VIEW");
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
						ResultSet resultdata = obj.view();
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnView.setBounds(529, 460, 85, 21);
		contentPane.add(btnView);
		
		JButton btnDelete = new JButton("DELETE");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = textField_ID.getText();
				int id1= Integer.parseInt(id);
				obj.delete(id1);
				try {
					ResultSet resultdata = obj.view();
				
			} catch (Exception e1) {
				e1.printStackTrace();
			}
				
			}
		});
		btnDelete.setBounds(720, 460, 85, 21);
		contentPane.add(btnDelete);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(273, 125, 616, 278);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
	}
}
