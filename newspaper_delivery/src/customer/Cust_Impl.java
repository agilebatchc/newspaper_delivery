package customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import connection.db_connection;

public class Cust_Impl {
	db_connection db = new db_connection();
	
	public String add_cust(String name,String contactno,String deliveryArea,String address) {
		
		String sql = "INSERT INTO Customer (C_name,Contact_no,Delivery_area,Address) VALUES (?, ?, ?, ?)";
		Connection con = db.getConn();
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, contactno);
			stmt.setString(3, deliveryArea);
			stmt.setString(4, address);
			int rowsInserted = stmt.executeUpdate();
			if (rowsInserted > 0) {
			    System.out.println("A new user was inserted successfully!");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	public ResultSet view() {
		
		String sql = "SELECT * FROM Customer";
		Connection con = db.getConn();
		Statement statement;
		ResultSet result = null;
		try {
			statement = con.createStatement();
			 result = statement.executeQuery(sql);
			 
//			while (result.next()){
//				int cid = result.getInt("C_id");
//			    String name = result.getString("C_name");
//			    String contactno = result.getString("Contact_no");
//			    String deliveryArea = result.getString("Delivery_area");
//			    String address = result.getString("Address");
//			 
//			    String output = "Customer #%d - %s - %s - %s - %s";
//			   System.out.println(String.format(output,cid, name, contactno, deliveryArea, address));
//			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	public String update( int cid ,String name,String contactno,String deliveryArea,String address) {
		
		String sql = "UPDATE Customer SET C_name=? ,Contact_no=?, Delivery_Area=?, Address=? WHERE C_id=? ";
		Connection con = db.getConn(); 
		PreparedStatement statement;
		try {
			statement = con.prepareStatement(sql);
		
			statement.setString(1, name);
			statement.setString(2, contactno);
			statement.setString(3, deliveryArea);
			statement.setString(4, address);
			statement.setInt(5, cid);
			 
			int rowsUpdated = statement.executeUpdate();
			if (rowsUpdated > 0) {
			    System.out.println("An existing customer was updated successfully!");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
		
	}
	
	public int delete(int i) {
		
		String sql = "DELETE FROM Customer WHERE C_id=?";
		Connection con = db.getConn(); 
		PreparedStatement statement;
		try {
			statement = con.prepareStatement(sql);
			statement.setInt(1, i);
			 
			int rowsDeleted = statement.executeUpdate();
			if (rowsDeleted > 0) {
			    System.out.println("A customer was deleted successfully!");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
		
	}
	
	
}

