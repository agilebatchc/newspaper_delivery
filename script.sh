rm -rf newspaper_delivery/bin 
mkdir newspaper_delivery/bin
find newspaper_delivery/src -type f -name '*.java' -exec javac -d newspaper_delivery/bin -cp "newspaper_delivery/lib/*" {} +


testCases=(`find newspaper_delivery/bin/test_cases -type f -name '*.class'`)

for test in "${testCases[@]}"
do
    filename=$(basename "$test")
    parent1="$(basename "$(dirname "$test")")"
    file="${filename%.*}"

    echo "$parent1.$file"
   
    java -cp newspaper_delivery/bin:"newspaper_delivery/lib/*" junit.textui.TestRunner "$parent1.$file"

    if [ $? -ne 0 ]; then
        echo "error"
    else
    	echo "yo  "
    fi   
done


#cd newspaper_delivery/bin
#find ./ -type f -name '*.class' -exec jar cfe Calculator.jar customer.cust_GUI {} +

find ./ -type f -printf "%f\n"